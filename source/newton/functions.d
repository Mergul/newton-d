/* Copyright (c) <2003-2016> <Julio Jerez, Newton Game Dynamics>
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
*
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
*
* 3. This notice may not be removed or altered from any source distribution.
*/
module newton.functions;

import newton.types;

extern(C):
nothrow:
@nogc:

// **********************************************************************************************
//
// world control functions
//
// **********************************************************************************************
int NewtonWorldGetVersion ();
int NewtonWorldFloatSize ();

int NewtonGetMemoryUsed ();
void NewtonSetMemorySystem (NewtonAllocMemory malloc, NewtonFreeMemory free);

NewtonWorld* NewtonCreate ();
void NewtonDestroy (const NewtonWorld* newtonWorld);
void NewtonDestroyAllBodies (const NewtonWorld* newtonWorld);

NewtonPostUpdateCallback NewtonGetPostUpdateCallback (const NewtonWorld* newtonWorld);
void NewtonSetPostUpdateCallback (const NewtonWorld* newtonWorld, NewtonPostUpdateCallback callback);

void* NewtonAlloc (int sizeInBytes);
void NewtonFree (void* ptr);

void NewtonLoadPlugins (const NewtonWorld* newtonWorld, const char* plugInPath);
void NewtonUnloadPlugins (const NewtonWorld* newtonWorld);
void* NewtonCurrentPlugin (const NewtonWorld* newtonWorld);
void* NewtonGetFirstPlugin (const NewtonWorld* newtonWorld);
void* NewtonGetPreferedPlugin (const NewtonWorld* newtonWorld);
void* NewtonGetNextPlugin (const NewtonWorld* newtonWorld, const void* plugin);
const(char)* NewtonGetPluginString (const NewtonWorld* newtonWorld, const void* plugin);
void NewtonSelectPlugin (const NewtonWorld* newtonWorld, const void* plugin);

dFloat NewtonGetContactMergeTolerance (const NewtonWorld* newtonWorld);
void NewtonSetContactMergeTolerance (const NewtonWorld* newtonWorld, dFloat tolerance);

void NewtonInvalidateCache (const NewtonWorld* newtonWorld);

void NewtonSetSolverIterations (const NewtonWorld* newtonWorld, int model);
int NewtonGetSolverIterations (const NewtonWorld* newtonWorld);

void NewtonSetParallelSolverOnLargeIsland (const NewtonWorld* newtonWorld, int mode);
int NewtonGetParallelSolverOnLargeIsland (const NewtonWorld* newtonWorld);

int NewtonGetBroadphaseAlgorithm (const NewtonWorld* newtonWorld);
void NewtonSelectBroadphaseAlgorithm (const NewtonWorld* newtonWorld, int algorithmType);
void NewtonResetBroadphase (const NewtonWorld* newtonWorld);

void NewtonUpdate (const NewtonWorld* newtonWorld, dFloat timestep);
void NewtonUpdateAsync (const NewtonWorld* newtonWorld, dFloat timestep);
void NewtonWaitForUpdateToFinish (const NewtonWorld* newtonWorld);

int NewtonGetNumberOfSubsteps (const NewtonWorld* newtonWorld);
void NewtonSetNumberOfSubsteps (const NewtonWorld* newtonWorld, int subSteps);
dFloat NewtonGetLastUpdateTime (const NewtonWorld* newtonWorld);

void NewtonSerializeToFile (const NewtonWorld* newtonWorld, const char* filename, NewtonOnBodySerializationCallback bodyCallback, void* bodyUserData);
void NewtonDeserializeFromFile (const NewtonWorld* newtonWorld, const char* filename, NewtonOnBodyDeserializationCallback bodyCallback, void* bodyUserData);

void NewtonSerializeScene (
    const NewtonWorld* newtonWorld,
    NewtonOnBodySerializationCallback bodyCallback,
    void* bodyUserData,
    NewtonSerializeCallback serializeCallback,
    void* serializeHandle);
void NewtonDeserializeScene (
    const NewtonWorld* newtonWorld,
    NewtonOnBodyDeserializationCallback bodyCallback,
    void* bodyUserData,
    NewtonDeserializeCallback serializeCallback,
    void* serializeHandle);

NewtonBody* NewtonFindSerializedBody (const NewtonWorld* newtonWorld, int bodySerializedID);
void NewtonSetJointSerializationCallbacks (const NewtonWorld* newtonWorld, NewtonOnJointSerializationCallback serializeJoint, NewtonOnJointDeserializationCallback deserializeJoint);
void NewtonGetJointSerializationCallbacks (const NewtonWorld* newtonWorld, NewtonOnJointSerializationCallback* serializeJoint, NewtonOnJointDeserializationCallback* deserializeJoint);

// multi threading interface 
void NewtonWorldCriticalSectionLock (const NewtonWorld* newtonWorld, int threadIndex);
void NewtonWorldCriticalSectionUnlock (const NewtonWorld* newtonWorld);
void NewtonSetThreadsCount (const NewtonWorld* newtonWorld, int threads);
int NewtonGetThreadsCount (const NewtonWorld* newtonWorld);
int NewtonGetMaxThreadsCount (const NewtonWorld* newtonWorld);
void NewtonDispachThreadJob (const NewtonWorld* newtonWorld, NewtonJobTask task, void* usedData, const char* functionName);
void NewtonSyncThreadJobs (const NewtonWorld* newtonWorld);

// atomic operations
int NewtonAtomicAdd (int* ptr, int value);
int NewtonAtomicSwap (int* ptr, int value);
void NewtonYield ();

void NewtonSetIslandUpdateEvent (const NewtonWorld* newtonWorld, NewtonIslandUpdate islandUpdate);
//	NEWTON_API void NewtonSetDestroyBodyByExeciveForce (const NewtonWorld* const newtonWorld, NewtonDestroyBodyByExeciveForce callback); 
//	NEWTON_API void NewtonWorldForEachBodyDo (const NewtonWorld* const newtonWorld, NewtonBodyIterator callback);
void NewtonWorldForEachJointDo (const NewtonWorld* newtonWorld, NewtonJointIterator callback, void* userData);
void NewtonWorldForEachBodyInAABBDo (const NewtonWorld* newtonWorld, const dFloat* p0, const dFloat* p1, NewtonBodyIterator callback, void* userData);

void NewtonWorldSetUserData (const NewtonWorld* newtonWorld, void* userData);
void* NewtonWorldGetUserData (const NewtonWorld* newtonWorld);

void* NewtonWorldAddListener (const NewtonWorld* newtonWorld, const char* nameId, void* listenerUserData);
void* NewtonWorldGetListener (const NewtonWorld* newtonWorld, const char* nameId);

void NewtonWorldListenerSetDestructorCallback (const NewtonWorld* newtonWorld, void* listener, NewtonWorldDestroyListenerCallback destroy);
void NewtonWorldListenerSetPreUpdateCallback (const NewtonWorld* newtonWorld, void* listener, NewtonWorldUpdateListenerCallback update);
void NewtonWorldListenerSetPostUpdateCallback (const NewtonWorld* newtonWorld, void* listener, NewtonWorldUpdateListenerCallback update);
void NewtonWorldListenerSetDebugCallback (const NewtonWorld* newtonWorld, void* listener, NewtonWorldListenerDebugCallback debugCallback);
void NewtonWorldListenerSetBodyDestroyCallback (const NewtonWorld* newtonWorld, void* listener, NewtonWorldListenerBodyDestroyCallback bodyDestroyCallback);
void NewtonWorldListenerDebug (const NewtonWorld* newtonWorld, void* context);
void* NewtonWorldGetListenerUserData (const NewtonWorld* newtonWorld, void* listener);
NewtonWorldListenerBodyDestroyCallback NewtonWorldListenerGetBodyDestroyCallback (const NewtonWorld* newtonWorld, void* listener);

void NewtonWorldSetDestructorCallback (const NewtonWorld* newtonWorld, NewtonWorldDestructorCallback destructor);
NewtonWorldDestructorCallback NewtonWorldGetDestructorCallback (const NewtonWorld* newtonWorld);
void NewtonWorldSetCollisionConstructorDestructorCallback (const NewtonWorld* newtonWorld, NewtonCollisionCopyConstructionCallback constructor, NewtonCollisionDestructorCallback destructor);

void NewtonWorldSetCreateDestroyContactCallback (const NewtonWorld* newtonWorld, NewtonCreateContactCallback createContact, NewtonDestroyContactCallback destroyContact);

void NewtonWorldRayCast (const NewtonWorld* newtonWorld, const dFloat* p0, const dFloat* p1, NewtonWorldRayFilterCallback filter, void* userData, NewtonWorldRayPrefilterCallback prefilter, int threadIndex);
int NewtonWorldConvexCast (const NewtonWorld* newtonWorld, const dFloat* matrix, const dFloat* target, const NewtonCollision* shape, dFloat* param, void* userData, NewtonWorldRayPrefilterCallback prefilter, NewtonWorldConvexCastReturnInfo* info, int maxContactsCount, int threadIndex);
int NewtonWorldCollide (const NewtonWorld* newtonWorld, const dFloat* matrix, const NewtonCollision* shape, void* userData, NewtonWorldRayPrefilterCallback prefilter, NewtonWorldConvexCastReturnInfo* info, int maxContactsCount, int threadIndex);

// world utility functions
int NewtonWorldGetBodyCount (const NewtonWorld* newtonWorld);
int NewtonWorldGetConstraintCount (const NewtonWorld* newtonWorld);

// **********************************************************************************************
//
// Simulation islands 
//
// **********************************************************************************************
NewtonBody* NewtonIslandGetBody (const void* island, int bodyIndex);
void NewtonIslandGetBodyAABB (const void* island, int bodyIndex, dFloat* p0, dFloat* p1);

// **********************************************************************************************
//
// Physics Material Section
//
// **********************************************************************************************
int NewtonMaterialCreateGroupID (const NewtonWorld* newtonWorld);
int NewtonMaterialGetDefaultGroupID (const NewtonWorld* newtonWorld);
void NewtonMaterialDestroyAllGroupID (const NewtonWorld* newtonWorld);

// material definitions that can not be overwritten in function callback
void* NewtonMaterialGetUserData (const NewtonWorld* newtonWorld, int id0, int id1);
void NewtonMaterialSetSurfaceThickness (const NewtonWorld* newtonWorld, int id0, int id1, dFloat thickness);

//	deprecated, not longer continue collision is set on the material  	
//	NEWTON_API void NewtonMaterialSetContinuousCollisionMode (const NewtonWorld* const newtonWorld, int id0, int id1, int state);

void NewtonMaterialSetCallbackUserData (const NewtonWorld* newtonWorld, int id0, int id1, void* userData);
void NewtonMaterialSetContactGenerationCallback (const NewtonWorld* newtonWorld, int id0, int id1, NewtonOnContactGeneration contactGeneration);
void NewtonMaterialSetCompoundCollisionCallback (const NewtonWorld* newtonWorld, int id0, int id1, NewtonOnCompoundSubCollisionAABBOverlap compoundAabbOverlap);
void NewtonMaterialSetCollisionCallback (const NewtonWorld* newtonWorld, int id0, int id1, NewtonOnAABBOverlap aabbOverlap, NewtonContactsProcess process);

void NewtonMaterialSetDefaultSoftness (const NewtonWorld* newtonWorld, int id0, int id1, dFloat value);
void NewtonMaterialSetDefaultElasticity (const NewtonWorld* newtonWorld, int id0, int id1, dFloat elasticCoef);
void NewtonMaterialSetDefaultCollidable (const NewtonWorld* newtonWorld, int id0, int id1, int state);
void NewtonMaterialSetDefaultFriction (const NewtonWorld* newtonWorld, int id0, int id1, dFloat staticFriction, dFloat kineticFriction);

NewtonMaterial* NewtonWorldGetFirstMaterial (const NewtonWorld* newtonWorld);
NewtonMaterial* NewtonWorldGetNextMaterial (const NewtonWorld* newtonWorld, const NewtonMaterial* material);

NewtonBody* NewtonWorldGetFirstBody (const NewtonWorld* newtonWorld);
NewtonBody* NewtonWorldGetNextBody (const NewtonWorld* newtonWorld, const NewtonBody* curBody);

// **********************************************************************************************
//
// Physics Contact control functions
//
// **********************************************************************************************
void* NewtonMaterialGetMaterialPairUserData (const NewtonMaterial* material);
uint NewtonMaterialGetContactFaceAttribute (const NewtonMaterial* material);
NewtonCollision* NewtonMaterialGetBodyCollidingShape (const NewtonMaterial* material, const NewtonBody* body_);
dFloat NewtonMaterialGetContactNormalSpeed (const NewtonMaterial* material);
void NewtonMaterialGetContactForce (const NewtonMaterial* material, const NewtonBody* body_, dFloat* force);
void NewtonMaterialGetContactPositionAndNormal (const NewtonMaterial* material, const NewtonBody* body_, dFloat* posit, dFloat* normal);
void NewtonMaterialGetContactTangentDirections (const NewtonMaterial* material, const NewtonBody* body_, dFloat* dir0, dFloat* dir1);
dFloat NewtonMaterialGetContactTangentSpeed (const NewtonMaterial* material, int index);
dFloat NewtonMaterialGetContactMaxNormalImpact (const NewtonMaterial* material);
dFloat NewtonMaterialGetContactMaxTangentImpact (const NewtonMaterial* material, int index);
dFloat NewtonMaterialGetContactPenetration (const NewtonMaterial* material);

void NewtonMaterialSetContactSoftness (const NewtonMaterial* material, dFloat softness);
void NewtonMaterialSetContactThickness (const NewtonMaterial* material, dFloat thickness);
void NewtonMaterialSetContactElasticity (const NewtonMaterial* material, dFloat restitution);
void NewtonMaterialSetContactFrictionState (const NewtonMaterial* material, int state, int index);
void NewtonMaterialSetContactFrictionCoef (const NewtonMaterial* material, dFloat staticFrictionCoef, dFloat kineticFrictionCoef, int index);

void NewtonMaterialSetContactNormalAcceleration (const NewtonMaterial* material, dFloat accel);
void NewtonMaterialSetContactNormalDirection (const NewtonMaterial* material, const dFloat* directionVector);
void NewtonMaterialSetContactPosition (const NewtonMaterial* material, const dFloat* position);

void NewtonMaterialSetContactTangentFriction (const NewtonMaterial* material, dFloat friction, int index);
void NewtonMaterialSetContactTangentAcceleration (const NewtonMaterial* material, dFloat accel, int index);
void NewtonMaterialContactRotateTangentDirections (const NewtonMaterial* material, const dFloat* directionVector);

//NEWTON_API dFloat NewtonMaterialGetContactPruningTolerance (const NewtonBody* body0, const NewtonBody* body1);
//NEWTON_API void NewtonMaterialSetContactPruningTolerance (const NewtonBody* body0, const NewtonBody* body1, dFloat tolerance);
dFloat NewtonMaterialGetContactPruningTolerance (const NewtonJoint* contactJoint);
void NewtonMaterialSetContactPruningTolerance (const NewtonJoint* contactJoint, dFloat tolerance);

// **********************************************************************************************
//
// convex collision primitives creation functions
//
// **********************************************************************************************
NewtonCollision* NewtonCreateNull (const NewtonWorld* newtonWorld);
NewtonCollision* NewtonCreateSphere (const NewtonWorld* newtonWorld, dFloat radius, int shapeID, const dFloat* offsetMatrix);
NewtonCollision* NewtonCreateBox (const NewtonWorld* newtonWorld, dFloat dx, dFloat dy, dFloat dz, int shapeID, const dFloat* offsetMatrix);
NewtonCollision* NewtonCreateCone (const NewtonWorld* newtonWorld, dFloat radius, dFloat height, int shapeID, const dFloat* offsetMatrix);
NewtonCollision* NewtonCreateCapsule (const NewtonWorld* newtonWorld, dFloat radius0, dFloat radius1, dFloat height, int shapeID, const dFloat* offsetMatrix);
NewtonCollision* NewtonCreateCylinder (const NewtonWorld* newtonWorld, dFloat radio0, dFloat radio1, dFloat height, int shapeID, const dFloat* offsetMatrix);
NewtonCollision* NewtonCreateChamferCylinder (const NewtonWorld* newtonWorld, dFloat radius, dFloat height, int shapeID, const dFloat* offsetMatrix);
NewtonCollision* NewtonCreateConvexHull (const NewtonWorld* newtonWorld, int count, const dFloat* vertexCloud, int strideInBytes, dFloat tolerance, int shapeID, const dFloat* offsetMatrix);
NewtonCollision* NewtonCreateConvexHullFromMesh (const NewtonWorld* newtonWorld, const NewtonMesh* mesh, dFloat tolerance, int shapeID);

int NewtonCollisionGetMode (const NewtonCollision* convexCollision);
void NewtonCollisionSetMode (const NewtonCollision* convexCollision, int mode);

//	NEWTON_API void NewtonCollisionSetMaxBreakImpactImpulse(const NewtonCollision* const convexHullCollision, dFloat maxImpactImpulse);
//	NEWTON_API dFloat NewtonCollisionGetMaxBreakImpactImpulse(const NewtonCollision* const convexHullCollision);

int NewtonConvexHullGetFaceIndices (const NewtonCollision* convexHullCollision, int face, int* faceIndices);
int NewtonConvexHullGetVertexData (const NewtonCollision* convexHullCollision, dFloat** vertexData, int* strideInBytes);

dFloat NewtonConvexCollisionCalculateVolume (const NewtonCollision* convexCollision);
void NewtonConvexCollisionCalculateInertialMatrix (const(NewtonCollision)* convexCollision, dFloat* inertia, dFloat* origin);
dFloat NewtonConvexCollisionCalculateBuoyancyVolume (const NewtonCollision* convexCollision, const dFloat* matrix, const dFloat* fluidPlane, dFloat* centerOfBuoyancy);

const(void)* NewtonCollisionDataPointer (const NewtonCollision* convexCollision);

// **********************************************************************************************
//
// compound collision primitives creation functions
//
// **********************************************************************************************
NewtonCollision* NewtonCreateCompoundCollision (const NewtonWorld* newtonWorld, int shapeID);
NewtonCollision* NewtonCreateCompoundCollisionFromMesh (const NewtonWorld* newtonWorld, const NewtonMesh* mesh, dFloat hullTolerance, int shapeID, int subShapeID);

void NewtonCompoundCollisionBeginAddRemove (NewtonCollision* compoundCollision);
void* NewtonCompoundCollisionAddSubCollision (NewtonCollision* compoundCollision, const NewtonCollision* convexCollision);
void NewtonCompoundCollisionRemoveSubCollision (NewtonCollision* compoundCollision, const void* collisionNode);
void NewtonCompoundCollisionRemoveSubCollisionByIndex (NewtonCollision* compoundCollision, int nodeIndex);
void NewtonCompoundCollisionSetSubCollisionMatrix (NewtonCollision* compoundCollision, const void* collisionNode, const dFloat* matrix);
void NewtonCompoundCollisionEndAddRemove (NewtonCollision* compoundCollision);

void* NewtonCompoundCollisionGetFirstNode (NewtonCollision* compoundCollision);
void* NewtonCompoundCollisionGetNextNode (NewtonCollision* compoundCollision, const void* collisionNode);

void* NewtonCompoundCollisionGetNodeByIndex (NewtonCollision* compoundCollision, int index);
int NewtonCompoundCollisionGetNodeIndex (NewtonCollision* compoundCollision, const void* collisionNode);
NewtonCollision* NewtonCompoundCollisionGetCollisionFromNode (NewtonCollision* compoundCollision, const void* collisionNode);

// **********************************************************************************************
//
// Fractured compound collision primitives interface
//
// **********************************************************************************************
NewtonCollision* NewtonCreateFracturedCompoundCollision (
    const NewtonWorld* newtonWorld,
    const NewtonMesh* solidMesh,
    int shapeID,
    int fracturePhysicsMaterialID,
    int pointcloudCount,
    const dFloat* vertexCloud,
    int strideInBytes,
    int materialID,
    const dFloat* textureMatrix,
    NewtonFractureCompoundCollisionReconstructMainMeshCallBack regenerateMainMeshCallback,
    NewtonFractureCompoundCollisionOnEmitCompoundFractured emitFracturedCompound,
    NewtonFractureCompoundCollisionOnEmitChunk emitFracfuredChunk);
NewtonCollision* NewtonFracturedCompoundPlaneClip (const NewtonCollision* fracturedCompound, const dFloat* plane);

void NewtonFracturedCompoundSetCallbacks (
    const NewtonCollision* fracturedCompound,
    NewtonFractureCompoundCollisionReconstructMainMeshCallBack regenerateMainMeshCallback,
    NewtonFractureCompoundCollisionOnEmitCompoundFractured emitFracturedCompound,
    NewtonFractureCompoundCollisionOnEmitChunk emitFracfuredChunk);

int NewtonFracturedCompoundIsNodeFreeToDetach (const NewtonCollision* fracturedCompound, void* collisionNode);
int NewtonFracturedCompoundNeighborNodeList (const NewtonCollision* fracturedCompound, void* collisionNode, void** list, int maxCount);

NewtonFracturedCompoundMeshPart* NewtonFracturedCompoundGetMainMesh (const NewtonCollision* fracturedCompound);
NewtonFracturedCompoundMeshPart* NewtonFracturedCompoundGetFirstSubMesh (const NewtonCollision* fracturedCompound);
NewtonFracturedCompoundMeshPart* NewtonFracturedCompoundGetNextSubMesh (const NewtonCollision* fracturedCompound, NewtonFracturedCompoundMeshPart* subMesh);

int NewtonFracturedCompoundCollisionGetVertexCount (const NewtonCollision* fracturedCompound, const NewtonFracturedCompoundMeshPart* meshOwner);
const(dFloat)* NewtonFracturedCompoundCollisionGetVertexPositions (const NewtonCollision* fracturedCompound, const NewtonFracturedCompoundMeshPart* meshOwner);
const(dFloat)* NewtonFracturedCompoundCollisionGetVertexNormals (const NewtonCollision* fracturedCompound, const NewtonFracturedCompoundMeshPart* meshOwner);
const(dFloat)* NewtonFracturedCompoundCollisionGetVertexUVs (const NewtonCollision* fracturedCompound, const NewtonFracturedCompoundMeshPart* meshOwner);
int NewtonFracturedCompoundMeshPartGetIndexStream (const NewtonCollision* fracturedCompound, const NewtonFracturedCompoundMeshPart* meshOwner, const void* segment, int* index);

void* NewtonFracturedCompoundMeshPartGetFirstSegment (const NewtonFracturedCompoundMeshPart* fractureCompoundMeshPart);
void* NewtonFracturedCompoundMeshPartGetNextSegment (const void* fractureCompoundMeshSegment);
int NewtonFracturedCompoundMeshPartGetMaterial (const void* fractureCompoundMeshSegment);
int NewtonFracturedCompoundMeshPartGetIndexCount (const void* fractureCompoundMeshSegment);

// **********************************************************************************************
//
// scene collision are static compound collision that can take polygonal static collisions
//
// **********************************************************************************************
NewtonCollision* NewtonCreateSceneCollision (const NewtonWorld* newtonWorld, int shapeID);

void NewtonSceneCollisionBeginAddRemove (NewtonCollision* sceneCollision);
void* NewtonSceneCollisionAddSubCollision (NewtonCollision* sceneCollision, const NewtonCollision* collision);
void NewtonSceneCollisionRemoveSubCollision (NewtonCollision* compoundCollision, const void* collisionNode);
void NewtonSceneCollisionRemoveSubCollisionByIndex (NewtonCollision* sceneCollision, int nodeIndex);
void NewtonSceneCollisionSetSubCollisionMatrix (NewtonCollision* sceneCollision, const void* collisionNode, const dFloat* matrix);
void NewtonSceneCollisionEndAddRemove (NewtonCollision* sceneCollision);

void* NewtonSceneCollisionGetFirstNode (NewtonCollision* sceneCollision);
void* NewtonSceneCollisionGetNextNode (NewtonCollision* sceneCollision, const void* collisionNode);

void* NewtonSceneCollisionGetNodeByIndex (NewtonCollision* sceneCollision, int index);
int NewtonSceneCollisionGetNodeIndex (NewtonCollision* sceneCollision, const void* collisionNode);
NewtonCollision* NewtonSceneCollisionGetCollisionFromNode (NewtonCollision* sceneCollision, const void* collisionNode);

//  ***********************************************************************************************************
//
//	User Static mesh collision interface
//
// ***********************************************************************************************************
NewtonCollision* NewtonCreateUserMeshCollision (
    const NewtonWorld* newtonWorld,
    const dFloat* minBox,
    const dFloat* maxBox,
    void* userData,
    NewtonUserMeshCollisionCollideCallback collideCallback,
    NewtonUserMeshCollisionRayHitCallback rayHitCallback,
    NewtonUserMeshCollisionDestroyCallback destroyCallback,
    NewtonUserMeshCollisionGetCollisionInfo getInfoCallback,
    NewtonUserMeshCollisionAABBTest getLocalAABBCallback,
    NewtonUserMeshCollisionGetFacesInAABB facesInAABBCallback,
    NewtonOnUserCollisionSerializationCallback serializeCallback,
    int shapeID);

int NewtonUserMeshCollisionContinuousOverlapTest (const NewtonUserMeshCollisionCollideDesc* collideDescData, const void* continueCollisionHandle, const dFloat* minAabb, const dFloat* maxAabb);

//  ***********************************************************************************************************
//
//	Collision serialization functions
//
// ***********************************************************************************************************
NewtonCollision* NewtonCreateCollisionFromSerialization (const NewtonWorld* newtonWorld, NewtonDeserializeCallback deserializeFunction, void* serializeHandle);
void NewtonCollisionSerialize (const NewtonWorld* newtonWorld, const NewtonCollision* collision, NewtonSerializeCallback serializeFunction, void* serializeHandle);
void NewtonCollisionGetInfo (const NewtonCollision* collision, NewtonCollisionInfoRecord* collisionInfo);

// **********************************************************************************************
//
// Static collision shapes functions
//
// **********************************************************************************************
NewtonCollision* NewtonCreateHeightFieldCollision (const NewtonWorld* newtonWorld, int width, int height, int gridsDiagonals, int elevationdatType, const void* elevationMap, const char* attributeMap, dFloat verticalScale, dFloat horizontalScale_x, dFloat horizontalScale_z, int shapeID);
void NewtonHeightFieldSetUserRayCastCallback (const NewtonCollision* heightfieldCollision, NewtonHeightFieldRayCastCallback rayHitCallback);
void NewtonHeightFieldSetHorizontalDisplacement (const NewtonCollision* heightfieldCollision, const ushort* horizontalMap, dFloat scale);

NewtonCollision* NewtonCreateTreeCollision (const NewtonWorld* newtonWorld, int shapeID);
NewtonCollision* NewtonCreateTreeCollisionFromMesh (const NewtonWorld* newtonWorld, const NewtonMesh* mesh, int shapeID);
void NewtonTreeCollisionSetUserRayCastCallback (const NewtonCollision* treeCollision, NewtonCollisionTreeRayCastCallback rayHitCallback);

void NewtonTreeCollisionBeginBuild (const NewtonCollision* treeCollision);
void NewtonTreeCollisionAddFace (const NewtonCollision* treeCollision, int vertexCount, const dFloat* vertexPtr, int strideInBytes, int faceAttribute);
void NewtonTreeCollisionEndBuild (const NewtonCollision* treeCollision, int optimize);

int NewtonTreeCollisionGetFaceAttribute (const NewtonCollision* treeCollision, const int* faceIndexArray, int indexCount);
void NewtonTreeCollisionSetFaceAttribute (const NewtonCollision* treeCollision, const int* faceIndexArray, int indexCount, int attribute);

void NewtonTreeCollisionForEachFace (const NewtonCollision* treeCollision, NewtonTreeCollisionFaceCallback forEachFaceCallback, void* context);

int NewtonTreeCollisionGetVertexListTriangleListInAABB (const NewtonCollision* treeCollision, const dFloat* p0, const dFloat* p1, const dFloat** vertexArray, int* vertexCount, int* vertexStrideInBytes, const int* indexList, int maxIndexCount, const int* faceAttribute);

void NewtonStaticCollisionSetDebugCallback (const NewtonCollision* staticCollision, NewtonTreeCollisionCallback userCallback);

// **********************************************************************************************
//
// General purpose collision library functions
//
// **********************************************************************************************
NewtonCollision* NewtonCollisionCreateInstance (const NewtonCollision* collision);
int NewtonCollisionGetType (const NewtonCollision* collision);
int NewtonCollisionIsConvexShape (const NewtonCollision* collision);
int NewtonCollisionIsStaticShape (const NewtonCollision* collision);

// for the end user
void NewtonCollisionSetUserData (const NewtonCollision* collision, void* userData);
void* NewtonCollisionGetUserData (const NewtonCollision* collision);

void NewtonCollisionSetUserID (const NewtonCollision* collision, uint id);
uint NewtonCollisionGetUserID (const NewtonCollision* collision);

void NewtonCollisionGetMaterial (const NewtonCollision* collision, NewtonCollisionMaterial* userData);
void NewtonCollisionSetMaterial (const NewtonCollision* collision, const NewtonCollisionMaterial* userData);

void* NewtonCollisionGetSubCollisionHandle (const NewtonCollision* collision);
NewtonCollision* NewtonCollisionGetParentInstance (const NewtonCollision* collision);

void NewtonCollisionSetMatrix (const NewtonCollision* collision, const dFloat* matrix);
void NewtonCollisionGetMatrix (const NewtonCollision* collision, dFloat* matrix);

void NewtonCollisionSetScale (const NewtonCollision* collision, dFloat scaleX, dFloat scaleY, dFloat scaleZ);
void NewtonCollisionGetScale (const NewtonCollision* collision, dFloat* scaleX, dFloat* scaleY, dFloat* scaleZ);
void NewtonDestroyCollision (const NewtonCollision* collision);

dFloat NewtonCollisionGetSkinThickness (const NewtonCollision* collision);
void NewtonCollisionSetSkinThickness (const NewtonCollision* collision, dFloat thickness);

int NewtonCollisionIntersectionTest (
    const NewtonWorld* newtonWorld,
    const NewtonCollision* collisionA,
    const dFloat* matrixA,
    const NewtonCollision* collisionB,
    const dFloat* matrixB,
    int threadIndex);

int NewtonCollisionPointDistance (
    const NewtonWorld* newtonWorld,
    const dFloat* point,
    const NewtonCollision* collision,
    const dFloat* matrix,
    dFloat* contact,
    dFloat* normal,
    int threadIndex);

int NewtonCollisionClosestPoint (
    const NewtonWorld* newtonWorld,
    const NewtonCollision* collisionA,
    const dFloat* matrixA,
    const NewtonCollision* collisionB,
    const dFloat* matrixB,
    dFloat* contactA,
    dFloat* contactB,
    dFloat* normalAB,
    int threadIndex);

int NewtonCollisionCollide (
    const NewtonWorld* newtonWorld,
    int maxSize,
    const NewtonCollision* collisionA,
    const dFloat* matrixA,
    const NewtonCollision* collisionB,
    const dFloat* matrixB,
    dFloat* contacts,
    dFloat* normals,
    dFloat* penetration,
    long* attributeA,
    long* attributeB,
    int threadIndex);

int NewtonCollisionCollideContinue (
    const NewtonWorld* newtonWorld,
    int maxSize,
    dFloat timestep,
    const NewtonCollision* collisionA,
    const dFloat* matrixA,
    const dFloat* velocA,
    const(dFloat)* omegaA,
    const NewtonCollision* collisionB,
    const dFloat* matrixB,
    const dFloat* velocB,
    const dFloat* omegaB,
    dFloat* timeOfImpact,
    dFloat* contacts,
    dFloat* normals,
    dFloat* penetration,
    long* attributeA,
    long* attributeB,
    int threadIndex);

void NewtonCollisionSupportVertex (const NewtonCollision* collision, const dFloat* dir, dFloat* vertex);
dFloat NewtonCollisionRayCast (const NewtonCollision* collision, const dFloat* p0, const dFloat* p1, dFloat* normal, long* attribute);
void NewtonCollisionCalculateAABB (const NewtonCollision* collision, const dFloat* matrix, dFloat* p0, dFloat* p1);
void NewtonCollisionForEachPolygonDo (const NewtonCollision* collision, const dFloat* matrix, NewtonCollisionIterator callback, void* userData);

// **********************************************************************************************
// 
// collision aggregates, are a collision node on eh broad phase the serve as the root nod for a collection of rigid bodies
// that shared the property of being in close proximity all the time, they are similar to compound collision by the group bodies instead of collision instances
// These are good for speeding calculation calculation of rag doll, Vehicles or contractions of rigid bodied lined by joints.
// also for example if you know that many the life time of a group of bodies like the object on a house of a building will be localize to the confide of the building
// then warping the bodies under an aggregate will reduce collision calculation of almost an order of magnitude.
//
// **********************************************************************************************
void* NewtonCollisionAggregateCreate (NewtonWorld* world);
void NewtonCollisionAggregateDestroy (void* aggregate);
void NewtonCollisionAggregateAddBody (void* aggregate, const NewtonBody* body_);
void NewtonCollisionAggregateRemoveBody (void* aggregate, const NewtonBody* body_);

int NewtonCollisionAggregateGetSelfCollision (void* aggregate);
void NewtonCollisionAggregateSetSelfCollision (void* aggregate, int state);

// **********************************************************************************************
//
// transforms utility functions
//
// **********************************************************************************************
void NewtonSetEulerAngle (const dFloat* eulersAngles, dFloat* matrix);
void NewtonGetEulerAngle (const dFloat* matrix, dFloat* eulersAngles0, dFloat* eulersAngles1);
dFloat NewtonCalculateSpringDamperAcceleration (dFloat dt, dFloat ks, dFloat x, dFloat kd, dFloat s);

// **********************************************************************************************
//
// body manipulation functions
//
// **********************************************************************************************
NewtonBody* NewtonCreateDynamicBody (const NewtonWorld* newtonWorld, const NewtonCollision* collision, const dFloat* matrix);
NewtonBody* NewtonCreateKinematicBody (const NewtonWorld* newtonWorld, const NewtonCollision* collision, const dFloat* matrix);
NewtonBody* NewtonCreateAsymetricDynamicBody (const NewtonWorld* newtonWorld, const NewtonCollision* collision, const dFloat* matrix);

void NewtonDestroyBody (const NewtonBody* body_);

int NewtonBodyGetSimulationState (const NewtonBody* body_);
void NewtonBodySetSimulationState (const NewtonBody* bodyPtr, const int state);

int NewtonBodyGetType (const NewtonBody* body_);
int NewtonBodyGetCollidable (const NewtonBody* body_);
void NewtonBodySetCollidable (const NewtonBody* body_, int collidableState);

void NewtonBodyAddForce (const NewtonBody* body_, const dFloat* force);
void NewtonBodyAddTorque (const NewtonBody* body_, const dFloat* torque);
void NewtonBodyCalculateInverseDynamicsForce (const NewtonBody* body_, dFloat timestep, const dFloat* desiredVeloc, dFloat* forceOut);

void NewtonBodySetCentreOfMass (const NewtonBody* body_, const dFloat* com);
void NewtonBodySetMassMatrix (const NewtonBody* body_, dFloat mass, dFloat Ixx, dFloat Iyy, dFloat Izz);
void NewtonBodySetFullMassMatrix (const NewtonBody* body_, dFloat mass, const dFloat* inertiaMatrix);

void NewtonBodySetMassProperties (const NewtonBody* body_, dFloat mass, const NewtonCollision* collision);
void NewtonBodySetMatrix (const NewtonBody* body_, const dFloat* matrix);
void NewtonBodySetMatrixNoSleep (const NewtonBody* body_, const dFloat* matrix);
void NewtonBodySetMatrixRecursive (const NewtonBody* body_, const dFloat* matrix);

void NewtonBodySetMaterialGroupID (const NewtonBody* body_, int id);
void NewtonBodySetContinuousCollisionMode (const NewtonBody* body_, uint state);
void NewtonBodySetJointRecursiveCollision (const NewtonBody* body_, uint state);
void NewtonBodySetOmega (const NewtonBody* body_, const dFloat* omega);
void NewtonBodySetOmegaNoSleep (const NewtonBody* body_, const dFloat* omega);
void NewtonBodySetVelocity (const NewtonBody* body_, const dFloat* velocity);
void NewtonBodySetVelocityNoSleep (const NewtonBody* body_, const dFloat* velocity);
void NewtonBodySetForce (const NewtonBody* body_, const dFloat* force);
void NewtonBodySetTorque (const NewtonBody* body_, const dFloat* torque);

void NewtonBodySetLinearDamping (const NewtonBody* body_, dFloat linearDamp);
void NewtonBodySetAngularDamping (const NewtonBody* body_, const dFloat* angularDamp);
void NewtonBodySetCollision (const NewtonBody* body_, const NewtonCollision* collision);
void NewtonBodySetCollisionScale (const NewtonBody* body_, dFloat scaleX, dFloat scaleY, dFloat scaleZ);

int NewtonBodyGetSleepState (const NewtonBody* body_);
void NewtonBodySetSleepState (const NewtonBody* body_, int state);

int NewtonBodyGetAutoSleep (const NewtonBody* body_);
void NewtonBodySetAutoSleep (const NewtonBody* body_, int state);

int NewtonBodyGetFreezeState (const NewtonBody* body_);
void NewtonBodySetFreezeState (const NewtonBody* body_, int state);

int NewtonBodyGetGyroscopicTorque (const NewtonBody* body_);
void NewtonBodySetGyroscopicTorque (const NewtonBody* body_, int state);

void NewtonBodySetDestructorCallback (const NewtonBody* body_, NewtonBodyDestructor callback);
NewtonBodyDestructor NewtonBodyGetDestructorCallback (const NewtonBody* body_);

void NewtonBodySetTransformCallback (const NewtonBody* body_, NewtonSetTransform callback);
NewtonSetTransform NewtonBodyGetTransformCallback (const NewtonBody* body_);

void NewtonBodySetForceAndTorqueCallback (const NewtonBody* body_, NewtonApplyForceAndTorque callback);
NewtonApplyForceAndTorque NewtonBodyGetForceAndTorqueCallback (const NewtonBody* body_);

int NewtonBodyGetID (const NewtonBody* body_);

void NewtonBodySetUserData (const NewtonBody* body_, void* userData);
void* NewtonBodyGetUserData (const NewtonBody* body_);

NewtonWorld* NewtonBodyGetWorld (const NewtonBody* body_);
NewtonCollision* NewtonBodyGetCollision (const NewtonBody* body_);
int NewtonBodyGetMaterialGroupID (const NewtonBody* body_);

int NewtonBodyGetSerializedID (const NewtonBody* body_);
int NewtonBodyGetContinuousCollisionMode (const NewtonBody* body_);
int NewtonBodyGetJointRecursiveCollision (const NewtonBody* body_);

void NewtonBodyGetPosition (const NewtonBody* body_, dFloat* pos);
void NewtonBodyGetMatrix (const NewtonBody* body_, dFloat* matrix);
void NewtonBodyGetRotation (const NewtonBody* body_, dFloat* rotation);
void NewtonBodyGetMass (const NewtonBody* body_, dFloat* mass, dFloat* Ixx, dFloat* Iyy, dFloat* Izz);
void NewtonBodyGetInvMass (const NewtonBody* body_, dFloat* invMass, dFloat* invIxx, dFloat* invIyy, dFloat* invIzz);
void NewtonBodyGetInertiaMatrix (const NewtonBody* body_, dFloat* inertiaMatrix);
void NewtonBodyGetInvInertiaMatrix (const NewtonBody* body_, dFloat* invInertiaMatrix);
void NewtonBodyGetOmega (const NewtonBody* body_, dFloat* vector);
void NewtonBodyGetVelocity (const NewtonBody* body_, dFloat* vector);
void NewtonBodyGetAlpha (const NewtonBody* body_, dFloat* vector);
void NewtonBodyGetAcceleration (const NewtonBody* body_, dFloat* vector);
void NewtonBodyGetForce (const NewtonBody* body_, dFloat* vector);
void NewtonBodyGetTorque (const NewtonBody* body_, dFloat* vector);
void NewtonBodyGetCentreOfMass (const NewtonBody* body_, dFloat* com);
void NewtonBodyGetPointVelocity (const NewtonBody* body_, const dFloat* point, dFloat* velocOut);

void NewtonBodyApplyImpulsePair (const NewtonBody* body_, dFloat* linearImpulse, dFloat* angularImpulse, dFloat timestep);
void NewtonBodyAddImpulse (const NewtonBody* body_, const dFloat* pointDeltaVeloc, const dFloat* pointPosit, dFloat timestep);
void NewtonBodyApplyImpulseArray (const NewtonBody* body_, int impuleCount, int strideInByte, const dFloat* impulseArray, const dFloat* pointArray, dFloat timestep);

void NewtonBodyIntegrateVelocity (const NewtonBody* body_, dFloat timestep);

dFloat NewtonBodyGetLinearDamping (const NewtonBody* body_);
void NewtonBodyGetAngularDamping (const NewtonBody* body_, dFloat* vector);
void NewtonBodyGetAABB (const NewtonBody* body_, dFloat* p0, dFloat* p1);

NewtonJoint* NewtonBodyGetFirstJoint (const NewtonBody* body_);
NewtonJoint* NewtonBodyGetNextJoint (const NewtonBody* body_, const NewtonJoint* joint);

NewtonJoint* NewtonBodyGetFirstContactJoint (const NewtonBody* body_);
NewtonJoint* NewtonBodyGetNextContactJoint (const NewtonBody* body_, const NewtonJoint* contactJoint);
NewtonJoint* NewtonBodyFindContact (const NewtonBody* body0, const NewtonBody* body1);

// **********************************************************************************************
//
// contact joints interface
//
// **********************************************************************************************
void* NewtonContactJointGetFirstContact (const NewtonJoint* contactJoint);
void* NewtonContactJointGetNextContact (const NewtonJoint* contactJoint, void* contact);

int NewtonContactJointGetContactCount (const NewtonJoint* contactJoint);
void NewtonContactJointRemoveContact (const NewtonJoint* contactJoint, void* contact);

dFloat NewtonContactJointGetClosestDistance (const NewtonJoint* contactJoint);
void NewtonContactJointResetSelftJointCollision (const NewtonJoint* contactJoint);
void NewtonContactJointResetIntraJointCollision (const NewtonJoint* contactJoint);

NewtonMaterial* NewtonContactGetMaterial (const void* contact);

NewtonCollision* NewtonContactGetCollision0 (const void* contact);
NewtonCollision* NewtonContactGetCollision1 (const void* contact);

void* NewtonContactGetCollisionID0 (const void* contact);
void* NewtonContactGetCollisionID1 (const void* contact);

// **********************************************************************************************
//
// Common joint functions
//
// **********************************************************************************************
void* NewtonJointGetUserData (const NewtonJoint* joint);
void NewtonJointSetUserData (const NewtonJoint* joint, void* userData);

NewtonBody* NewtonJointGetBody0 (const NewtonJoint* joint);
NewtonBody* NewtonJointGetBody1 (const NewtonJoint* joint);

void NewtonJointGetInfo (const NewtonJoint* joint, NewtonJointRecord* info);
int NewtonJointGetCollisionState (const NewtonJoint* joint);
void NewtonJointSetCollisionState (const NewtonJoint* joint, int state);

dFloat NewtonJointGetStiffness (const NewtonJoint* joint);
void NewtonJointSetStiffness (const NewtonJoint* joint, dFloat state);

void NewtonDestroyJoint (const NewtonWorld* newtonWorld, const NewtonJoint* joint);
void NewtonJointSetDestructor (const NewtonJoint* joint, NewtonConstraintDestructor destructor);

int NewtonJointIsActive (const NewtonJoint* joint);

// **********************************************************************************************
//
// InverseDynamics Interface
//
// **********************************************************************************************
NewtonInverseDynamics* NewtonCreateInverseDynamics (const NewtonWorld* newtonWorld);
void NewtonInverseDynamicsDestroy (NewtonInverseDynamics* inverseDynamics);

void* NewtonInverseDynamicsGetRoot (NewtonInverseDynamics* inverseDynamics);
void* NewtonInverseDynamicsGetNextChildNode (NewtonInverseDynamics* inverseDynamics, void* node);
void* NewtonInverseDynamicsGetFirstChildNode (NewtonInverseDynamics* inverseDynamics, void* parentNode);

NewtonBody* NewtonInverseDynamicsGetBody (NewtonInverseDynamics* inverseDynamics, void* node);
NewtonJoint* NewtonInverseDynamicsGetJoint (NewtonInverseDynamics* inverseDynamics, void* node);

NewtonJoint* NewtonInverseDynamicsCreateEffector (NewtonInverseDynamics* inverseDynamics, void* node, NewtonUserBilateralCallback callback);
void NewtonInverseDynamicsDestroyEffector (NewtonJoint* effector);

void* NewtonInverseDynamicsAddRoot (NewtonInverseDynamics* inverseDynamics, NewtonBody* root);
void* NewtonInverseDynamicsAddChildNode (NewtonInverseDynamics* inverseDynamics, void* parentNode, NewtonJoint* joint);
bool NewtonInverseDynamicsAddLoopJoint (NewtonInverseDynamics* inverseDynamics, NewtonJoint* joint);

void NewtonInverseDynamicsEndBuild (NewtonInverseDynamics* inverseDynamics);

void NewtonInverseDynamicsUpdate (NewtonInverseDynamics* inverseDynamics, dFloat timestep, int threadIndex);

// **********************************************************************************************
//
// particle system interface (soft bodies, individual, pressure bodies and cloth)   
//
// **********************************************************************************************
NewtonCollision* NewtonCreateMassSpringDamperSystem (
    const NewtonWorld* newtonWorld,
    int shapeID,
    const dFloat* points,
    int pointCount,
    int strideInBytes,
    const dFloat* pointMass,
    const int* links,
    int linksCount,
    const dFloat* linksSpring,
    const dFloat* linksDamper);

NewtonCollision* NewtonCreateDeformableSolid (const NewtonWorld* newtonWorld, const NewtonMesh* mesh, int shapeID);

int NewtonDeformableMeshGetParticleCount (const NewtonCollision* deformableMesh);
int NewtonDeformableMeshGetParticleStrideInBytes (const NewtonCollision* deformableMesh);
const(dFloat)* NewtonDeformableMeshGetParticleArray (const NewtonCollision* deformableMesh);

/*
	NEWTON_API NewtonCollision* NewtonCreateClothPatch (const NewtonWorld* const newtonWorld, NewtonMesh* const mesh, int shapeID, NewtonClothPatchMaterial* const structuralMaterial, NewtonClothPatchMaterial* const bendMaterial);
	NEWTON_API void NewtonDeformableMeshCreateClusters (NewtonCollision* const deformableMesh, int clusterCount, dFloat overlapingWidth);
	NEWTON_API void NewtonDeformableMeshSetDebugCallback (NewtonCollision* const deformableMesh, NewtonCollisionIterator callback);


	NEWTON_API void NewtonDeformableMeshGetParticlePosition (NewtonCollision* const deformableMesh, int particleIndex, dFloat* const posit);

	NEWTON_API void NewtonDeformableMeshBeginConfiguration (const NewtonCollision* const deformableMesh);
	NEWTON_API void NewtonDeformableMeshUnconstraintParticle (NewtonCollision* const deformableMesh, int particleIndex);
	NEWTON_API void NewtonDeformableMeshConstraintParticle (NewtonCollision* const deformableMesh, int particleIndex, const dFloat* const posit, const NewtonBody* const body);
	NEWTON_API void NewtonDeformableMeshEndConfiguration (const NewtonCollision* const deformableMesh);

//	NEWTON_API void NewtonDeformableMeshSetPlasticity (NewtonCollision* const deformableMesh, dFloat plasticity);
//	NEWTON_API void NewtonDeformableMeshSetStiffness (NewtonCollision* const deformableMesh, dFloat stiffness);
	NEWTON_API void NewtonDeformableMeshSetSkinThickness (NewtonCollision* const deformableMesh, dFloat skinThickness);

	NEWTON_API void NewtonDeformableMeshUpdateRenderNormals (const NewtonCollision* const deformableMesh);
	NEWTON_API int NewtonDeformableMeshGetVertexCount (const NewtonCollision* const deformableMesh);
	NEWTON_API void NewtonDeformableMeshGetVertexStreams (const NewtonCollision* const deformableMesh, int vertexStrideInByte, dFloat* const vertex, int normalStrideInByte, dFloat* const normal, int uvStrideInByte0, dFloat* const uv0);
	NEWTON_API NewtonDeformableMeshSegment* NewtonDeformableMeshGetFirstSegment (const NewtonCollision* const deformableMesh);
	NEWTON_API NewtonDeformableMeshSegment* NewtonDeformableMeshGetNextSegment (const NewtonCollision* const deformableMesh, const NewtonDeformableMeshSegment* const segment);

	NEWTON_API int NewtonDeformableMeshSegmentGetMaterialID (const NewtonCollision* const deformableMesh, const NewtonDeformableMeshSegment* const segment);
	NEWTON_API int NewtonDeformableMeshSegmentGetIndexCount (const NewtonCollision* const deformableMesh, const NewtonDeformableMeshSegment* const segment);
	NEWTON_API const int* NewtonDeformableMeshSegmentGetIndexList (const NewtonCollision* const deformableMesh, const NewtonDeformableMeshSegment* const segment);
*/
// **********************************************************************************************
//
// Ball and Socket joint functions
//
// **********************************************************************************************
NewtonJoint* NewtonConstraintCreateBall (const NewtonWorld* newtonWorld, const(dFloat)* pivotPoint, const NewtonBody* childBody, const NewtonBody* parentBody);
void NewtonBallSetUserCallback (const NewtonJoint* ball, NewtonBallCallback callback);
void NewtonBallGetJointAngle (const NewtonJoint* ball, dFloat* angle);
void NewtonBallGetJointOmega (const NewtonJoint* ball, dFloat* omega);
void NewtonBallGetJointForce (const NewtonJoint* ball, dFloat* force);
void NewtonBallSetConeLimits (const NewtonJoint* ball, const(dFloat)* pin, dFloat maxConeAngle, dFloat maxTwistAngle);

// **********************************************************************************************
//
// Hinge joint functions
//
// **********************************************************************************************
NewtonJoint* NewtonConstraintCreateHinge (const NewtonWorld* newtonWorld, const(dFloat)* pivotPoint, const(dFloat)* pinDir, const NewtonBody* childBody, const NewtonBody* parentBody);
void NewtonHingeSetUserCallback (const NewtonJoint* hinge, NewtonHingeCallback callback);
dFloat NewtonHingeGetJointAngle (const NewtonJoint* hinge);
dFloat NewtonHingeGetJointOmega (const NewtonJoint* hinge);
void NewtonHingeGetJointForce (const NewtonJoint* hinge, dFloat* force);
dFloat NewtonHingeCalculateStopAlpha (const NewtonJoint* hinge, const NewtonHingeSliderUpdateDesc* desc, dFloat angle);

// **********************************************************************************************
//
// Slider joint functions
//
// **********************************************************************************************
NewtonJoint* NewtonConstraintCreateSlider (const NewtonWorld* newtonWorld, const(dFloat)* pivotPoint, const(dFloat)* pinDir, const NewtonBody* childBody, const NewtonBody* parentBody);
void NewtonSliderSetUserCallback (const NewtonJoint* slider, NewtonSliderCallback callback);
dFloat NewtonSliderGetJointPosit (const(NewtonJoint)* slider);
dFloat NewtonSliderGetJointVeloc (const(NewtonJoint)* slider);
void NewtonSliderGetJointForce (const NewtonJoint* slider, dFloat* force);
dFloat NewtonSliderCalculateStopAccel (const NewtonJoint* slider, const NewtonHingeSliderUpdateDesc* desc, dFloat position);

// **********************************************************************************************
//
// Corkscrew joint functions
//
// **********************************************************************************************
NewtonJoint* NewtonConstraintCreateCorkscrew (const NewtonWorld* newtonWorld, const(dFloat)* pivotPoint, const(dFloat)* pinDir, const NewtonBody* childBody, const NewtonBody* parentBody);
void NewtonCorkscrewSetUserCallback (const NewtonJoint* corkscrew, NewtonCorkscrewCallback callback);
dFloat NewtonCorkscrewGetJointPosit (const NewtonJoint* corkscrew);
dFloat NewtonCorkscrewGetJointAngle (const NewtonJoint* corkscrew);
dFloat NewtonCorkscrewGetJointVeloc (const NewtonJoint* corkscrew);
dFloat NewtonCorkscrewGetJointOmega (const NewtonJoint* corkscrew);
void NewtonCorkscrewGetJointForce (const NewtonJoint* corkscrew, dFloat* force);
dFloat NewtonCorkscrewCalculateStopAlpha (const NewtonJoint* corkscrew, const NewtonHingeSliderUpdateDesc* desc, dFloat angle);
dFloat NewtonCorkscrewCalculateStopAccel (const NewtonJoint* corkscrew, const NewtonHingeSliderUpdateDesc* desc, dFloat position);

// **********************************************************************************************
//
// Universal joint functions
//
// **********************************************************************************************
NewtonJoint* NewtonConstraintCreateUniversal (const NewtonWorld* newtonWorld, const(dFloat)* pivotPoint, const(dFloat)* pinDir0, const(dFloat)* pinDir1, const NewtonBody* childBody, const NewtonBody* parentBody);
void NewtonUniversalSetUserCallback (const NewtonJoint* universal, NewtonUniversalCallback callback);
dFloat NewtonUniversalGetJointAngle0 (const NewtonJoint* universal);
dFloat NewtonUniversalGetJointAngle1 (const NewtonJoint* universal);
dFloat NewtonUniversalGetJointOmega0 (const NewtonJoint* universal);
dFloat NewtonUniversalGetJointOmega1 (const NewtonJoint* universal);
void NewtonUniversalGetJointForce (const NewtonJoint* universal, dFloat* force);
dFloat NewtonUniversalCalculateStopAlpha0 (const NewtonJoint* universal, const NewtonHingeSliderUpdateDesc* desc, dFloat angle);
dFloat NewtonUniversalCalculateStopAlpha1 (const NewtonJoint* universal, const NewtonHingeSliderUpdateDesc* desc, dFloat angle);

// **********************************************************************************************
//
// Up vector joint functions
//
// **********************************************************************************************
NewtonJoint* NewtonConstraintCreateUpVector (const NewtonWorld* newtonWorld, const(dFloat)* pinDir, const NewtonBody* body_);
void NewtonUpVectorGetPin (const NewtonJoint* upVector, dFloat* pin);
void NewtonUpVectorSetPin (const NewtonJoint* upVector, const(dFloat)* pin);

// **********************************************************************************************
//
// User defined bilateral Joint
//
// **********************************************************************************************
NewtonJoint* NewtonConstraintCreateUserJoint (const NewtonWorld* newtonWorld, int maxDOF, NewtonUserBilateralCallback callback, const NewtonBody* childBody, const NewtonBody* parentBody);
int NewtonUserJointGetSolverModel (const NewtonJoint* joint);
void NewtonUserJointSetSolverModel (const NewtonJoint* joint, int model);
void NewtonUserJointSetFeedbackCollectorCallback (const NewtonJoint* joint, NewtonUserBilateralCallback getFeedback);
void NewtonUserJointAddLinearRow (const NewtonJoint* joint, const dFloat* pivot0, const dFloat* pivot1, const dFloat* dir);
void NewtonUserJointAddAngularRow (const NewtonJoint* joint, dFloat relativeAngle, const dFloat* dir);
void NewtonUserJointAddGeneralRow (const NewtonJoint* joint, const dFloat* jacobian0, const dFloat* jacobian1);
void NewtonUserJointSetRowMinimumFriction (const NewtonJoint* joint, dFloat friction);
void NewtonUserJointSetRowMaximumFriction (const NewtonJoint* joint, dFloat friction);
dFloat NewtonUserJointCalculateRowZeroAcceleration (const NewtonJoint* joint);
dFloat NewtonUserJointGetRowAcceleration (const NewtonJoint* joint);
void NewtonUserJointSetRowAsInverseDynamics (const NewtonJoint* joint);
void NewtonUserJointSetRowAcceleration (const NewtonJoint* joint, dFloat acceleration);
void NewtonUserJointSetRowSpringDamperAcceleration (const NewtonJoint* joint, dFloat rowStiffness, dFloat spring, dFloat damper);
void NewtonUserJointSetRowStiffness (const NewtonJoint* joint, dFloat stiffness);
int NewtonUserJoinRowsCount (const NewtonJoint* joint);
void NewtonUserJointGetGeneralRow (const NewtonJoint* joint, int index, dFloat* jacobian0, dFloat* jacobian1);
dFloat NewtonUserJointGetRowForce (const NewtonJoint* joint, int row);

int NewtonUserJointSubmitImmediateModeConstraint (const NewtonJoint* joint, NewtonImmediateModeConstraint* descriptor, dFloat timestep);

// **********************************************************************************************
//
// Mesh joint functions
//
// **********************************************************************************************
NewtonMesh* NewtonMeshCreate (const NewtonWorld* newtonWorld);
NewtonMesh* NewtonMeshCreateFromMesh (const NewtonMesh* mesh);
NewtonMesh* NewtonMeshCreateFromCollision (const NewtonCollision* collision);
NewtonMesh* NewtonMeshCreateTetrahedraIsoSurface (const NewtonMesh* mesh);
NewtonMesh* NewtonMeshCreateConvexHull (const NewtonWorld* newtonWorld, int pointCount, const dFloat* vertexCloud, int strideInBytes, dFloat tolerance);
NewtonMesh* NewtonMeshCreateVoronoiConvexDecomposition (const NewtonWorld* newtonWorld, int pointCount, const dFloat* vertexCloud, int strideInBytes, int materialID, const dFloat* textureMatrix);
NewtonMesh* NewtonMeshCreateFromSerialization (const NewtonWorld* newtonWorld, NewtonDeserializeCallback deserializeFunction, void* serializeHandle);
void NewtonMeshDestroy (const NewtonMesh* mesh);

void NewtonMeshSerialize (const NewtonMesh* mesh, NewtonSerializeCallback serializeFunction, void* serializeHandle);
void NewtonMeshSaveOFF (const NewtonMesh* mesh, const char* filename);
NewtonMesh* NewtonMeshLoadOFF (const NewtonWorld* newtonWorld, const char* filename);
NewtonMesh* NewtonMeshLoadTetrahedraMesh (const NewtonWorld* newtonWorld, const char* filename);

void NewtonMeshFlipWinding (const NewtonMesh* mesh);

void NewtonMeshApplyTransform (const NewtonMesh* mesh, const dFloat* matrix);
void NewtonMeshCalculateOOBB (const NewtonMesh* mesh, dFloat* matrix, dFloat* x, dFloat* y, dFloat* z);

void NewtonMeshCalculateVertexNormals (const NewtonMesh* mesh, dFloat angleInRadians);
void NewtonMeshApplySphericalMapping (const NewtonMesh* mesh, int material, const dFloat* aligmentMatrix);
void NewtonMeshApplyCylindricalMapping (const NewtonMesh* mesh, int cylinderMaterial, int capMaterial, const dFloat* aligmentMatrix);
void NewtonMeshApplyBoxMapping (const NewtonMesh* mesh, int frontMaterial, int sideMaterial, int topMaterial, const dFloat* aligmentMatrix);
void NewtonMeshApplyAngleBasedMapping (const NewtonMesh* mesh, int material, NewtonReportProgress reportPrograssCallback, void* reportPrgressUserData, dFloat* aligmentMatrix);

void NewtonCreateTetrahedraLinearBlendSkinWeightsChannel (const NewtonMesh* tetrahedraMesh, NewtonMesh* skinMesh);

void NewtonMeshOptimize (const NewtonMesh* mesh);
void NewtonMeshOptimizePoints (const NewtonMesh* mesh);
void NewtonMeshOptimizeVertex (const NewtonMesh* mesh);
int NewtonMeshIsOpenMesh (const NewtonMesh* mesh);
void NewtonMeshFixTJoints (const NewtonMesh* mesh);

void NewtonMeshPolygonize (const NewtonMesh* mesh);
void NewtonMeshTriangulate (const NewtonMesh* mesh);
NewtonMesh* NewtonMeshUnion (const NewtonMesh* mesh, const NewtonMesh* clipper, const dFloat* clipperMatrix);
NewtonMesh* NewtonMeshDifference (const NewtonMesh* mesh, const NewtonMesh* clipper, const dFloat* clipperMatrix);
NewtonMesh* NewtonMeshIntersection (const NewtonMesh* mesh, const NewtonMesh* clipper, const dFloat* clipperMatrix);
void NewtonMeshClip (const NewtonMesh* mesh, const NewtonMesh* clipper, const dFloat* clipperMatrix, NewtonMesh** topMesh, NewtonMesh** bottomMesh);

NewtonMesh* NewtonMeshConvexMeshIntersection (const NewtonMesh* mesh, const NewtonMesh* convexMesh);

NewtonMesh* NewtonMeshSimplify (const NewtonMesh* mesh, int maxVertexCount, NewtonReportProgress reportPrograssCallback, void* reportPrgressUserData);
NewtonMesh* NewtonMeshApproximateConvexDecomposition (const NewtonMesh* mesh, dFloat maxConcavity, dFloat backFaceDistanceFactor, int maxCount, int maxVertexPerHull, NewtonReportProgress reportProgressCallback, void* reportProgressUserData);

void NewtonRemoveUnusedVertices (const NewtonMesh* mesh, int* vertexRemapTable);

void NewtonMeshBeginBuild (const NewtonMesh* mesh);
void NewtonMeshBeginFace (const NewtonMesh* mesh);
void NewtonMeshAddPoint (const NewtonMesh* mesh, dFloat64 x, dFloat64 y, dFloat64 z);
void NewtonMeshAddLayer (const NewtonMesh* mesh, int layerIndex);
void NewtonMeshAddMaterial (const NewtonMesh* mesh, int materialIndex);
void NewtonMeshAddNormal (const NewtonMesh* mesh, dFloat x, dFloat y, dFloat z);
void NewtonMeshAddBinormal (const NewtonMesh* mesh, dFloat x, dFloat y, dFloat z);
void NewtonMeshAddUV0 (const NewtonMesh* mesh, dFloat u, dFloat v);
void NewtonMeshAddUV1 (const NewtonMesh* mesh, dFloat u, dFloat v);
void NewtonMeshAddVertexColor (const NewtonMesh* mesh, dFloat r, dFloat g, dFloat b, dFloat a);
void NewtonMeshEndFace (const NewtonMesh* mesh);
void NewtonMeshEndBuild (const NewtonMesh* mesh);

void NewtonMeshClearVertexFormat (NewtonMeshVertexFormat* format);
void NewtonMeshBuildFromVertexListIndexList (const NewtonMesh* mesh, const NewtonMeshVertexFormat* format);

int NewtonMeshGetPointCount (const NewtonMesh* mesh);
const(int)* NewtonMeshGetIndexToVertexMap (const NewtonMesh* mesh);

void NewtonMeshGetVertexDoubleChannel (const NewtonMesh* mesh, int vertexStrideInByte, dFloat64* outBuffer);
void NewtonMeshGetVertexChannel (const NewtonMesh* mesh, int vertexStrideInByte, dFloat* outBuffer);
void NewtonMeshGetNormalChannel (const NewtonMesh* mesh, int vertexStrideInByte, dFloat* outBuffer);
void NewtonMeshGetBinormalChannel (const NewtonMesh* mesh, int vertexStrideInByte, dFloat* outBuffer);
void NewtonMeshGetUV0Channel (const NewtonMesh* mesh, int vertexStrideInByte, dFloat* outBuffer);
void NewtonMeshGetUV1Channel (const NewtonMesh* mesh, int vertexStrideInByte, dFloat* outBuffer);
void NewtonMeshGetVertexColorChannel (const NewtonMesh* mesh, int vertexStrideInByte, dFloat* outBuffer);

int NewtonMeshHasNormalChannel (const NewtonMesh* mesh);
int NewtonMeshHasBinormalChannel (const NewtonMesh* mesh);
int NewtonMeshHasUV0Channel (const NewtonMesh* mesh);
int NewtonMeshHasUV1Channel (const NewtonMesh* mesh);
int NewtonMeshHasVertexColorChannel (const NewtonMesh* mesh);

void* NewtonMeshBeginHandle (const NewtonMesh* mesh);
void NewtonMeshEndHandle (const NewtonMesh* mesh, void* handle);
int NewtonMeshFirstMaterial (const NewtonMesh* mesh, void* handle);
int NewtonMeshNextMaterial (const NewtonMesh* mesh, void* handle, int materialId);
int NewtonMeshMaterialGetMaterial (const NewtonMesh* mesh, void* handle, int materialId);
int NewtonMeshMaterialGetIndexCount (const NewtonMesh* mesh, void* handle, int materialId);
void NewtonMeshMaterialGetIndexStream (const NewtonMesh* mesh, void* handle, int materialId, int* index);
void NewtonMeshMaterialGetIndexStreamShort (const NewtonMesh* mesh, void* handle, int materialId, short* index);

NewtonMesh* NewtonMeshCreateFirstSingleSegment (const NewtonMesh* mesh);
NewtonMesh* NewtonMeshCreateNextSingleSegment (const NewtonMesh* mesh, const NewtonMesh* segment);

NewtonMesh* NewtonMeshCreateFirstLayer (const NewtonMesh* mesh);
NewtonMesh* NewtonMeshCreateNextLayer (const NewtonMesh* mesh, const NewtonMesh* segment);

int NewtonMeshGetTotalFaceCount (const NewtonMesh* mesh);
int NewtonMeshGetTotalIndexCount (const NewtonMesh* mesh);
void NewtonMeshGetFaces (const NewtonMesh* mesh, int* faceIndexCount, int* faceMaterial, void** faceIndices);

int NewtonMeshGetVertexCount (const NewtonMesh* mesh);
int NewtonMeshGetVertexStrideInByte (const NewtonMesh* mesh);
const(dFloat64)* NewtonMeshGetVertexArray (const NewtonMesh* mesh);

int NewtonMeshGetVertexBaseCount (const NewtonMesh* mesh);
void NewtonMeshSetVertexBaseCount (const NewtonMesh* mesh, int baseCount);

void* NewtonMeshGetFirstVertex (const NewtonMesh* mesh);
void* NewtonMeshGetNextVertex (const NewtonMesh* mesh, const void* vertex);
int NewtonMeshGetVertexIndex (const NewtonMesh* mesh, const void* vertex);

void* NewtonMeshGetFirstPoint (const NewtonMesh* mesh);
void* NewtonMeshGetNextPoint (const NewtonMesh* mesh, const void* point);
int NewtonMeshGetPointIndex (const NewtonMesh* mesh, const void* point);
int NewtonMeshGetVertexIndexFromPoint (const NewtonMesh* mesh, const void* point);

void* NewtonMeshGetFirstEdge (const NewtonMesh* mesh);
void* NewtonMeshGetNextEdge (const NewtonMesh* mesh, const void* edge);
void NewtonMeshGetEdgeIndices (const NewtonMesh* mesh, const void* edge, int* v0, int* v1);
//void NewtonMeshGetEdgePointIndices (const NewtonMesh* const mesh, const void* const edge, int* const v0, int* const v1);

void* NewtonMeshGetFirstFace (const NewtonMesh* mesh);
void* NewtonMeshGetNextFace (const NewtonMesh* mesh, const void* face);
int NewtonMeshIsFaceOpen (const NewtonMesh* mesh, const void* face);
int NewtonMeshGetFaceMaterial (const NewtonMesh* mesh, const void* face);
int NewtonMeshGetFaceIndexCount (const NewtonMesh* mesh, const void* face);
void NewtonMeshGetFaceIndices (const NewtonMesh* mesh, const void* face, int* indices);
void NewtonMeshGetFacePointIndices (const NewtonMesh* mesh, const void* face, int* indices);
void NewtonMeshCalculateFaceNormal (const NewtonMesh* mesh, const void* face, dFloat64* normal);

void NewtonMeshSetFaceMaterial (const NewtonMesh* mesh, const void* face, int matId);

